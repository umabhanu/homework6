﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace homework6
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            CheckConnectivity(); // to check connectivity
        }
        

        private void CheckConnectivity() 
        {
            var isconnected = CrossConnectivity.Current.IsConnected; // built in method to check connectivity
            if (isconnected == true) //if true then it continues 
            {
                async void When_Clicked(object sender, System.EventArgs e)
                {
                    if (!string.IsNullOrWhiteSpace(entry11.Text)) // not to accept null values
                    {
                        // creates http client object
                        HttpClient client = new HttpClient();
                        string st = entry11.Text;
                        var uri = new Uri(string.Format($"https://owlbot.info/api/v2/dictionary/" + st));
                       //sends request's to the server when user enters the word 
                        var request = new HttpRequestMessage();
                        request.Method = HttpMethod.Get;
                        request.RequestUri = uri;
                        // server sends the requested data back to user after processing
                        HttpResponseMessage response = await client.SendAsync(request);
                        DictionaryData[] d = null;
                        if (response.IsSuccessStatusCode)
                        {
                            var content = await response.Content.ReadAsStringAsync();
                            d = DictionaryData.FromJson(content);
                            tType.Text = $"Type is {d[0].Type}";
                            tDefinition.Text = $"Word is {d[1].DefinitionDefinition}";
                            tExample.Text = $"Example: {d[2].Example}";
                        }
                    }
                }
            }




            else //(isconnected == false)
            {
                ConnectivityLabel.Text = "you are not connected"; // displays when connection is lost
            }

        }
        

    } 
}
